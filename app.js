 function myDropmenu() {
     var myLinks = document.getElementById('myLinks');
     if (myLinks.style.display === 'block') {
         myLinks.style.display = 'none';
     } else {
         myLinks.style.display = 'block';
     }
 }

 function validate() {
     let name = document.forms["form"]["name"].value;
     let email = document.forms["form"]["email"].value;

     if (name.length == 0) {
         document.getElementById("namef").innerHTML = "*данное поле обязательно для заполнения";
         return false;
     }

     if (email.length == 0) {
         document.getElementById("emailf").innerHTML = "*данное поле обязательно для заполнения";
         return false;
     }

     at = email.indexOf("@");
     dot = email.indexOf(".");

     if (at < 1 || dot < 1) {
         document.getElementById("emailf").innerHTML = "*email введен не верно";
         return false;
     }
 }


 function openTab(event, tabName) {
     let tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     }
     let tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace("active", "");
     }
     document.getElementById(tabName).style.display = "block";
     event.currentTarget.className += " active";

 }


 let slides = document.querySelectorAll('#slides .slide');
 let currentSlide = 0;
 let slideInterval = setInterval(nextSlide, 2000);

 function nextSlide() {
     slides[currentSlide].className = 'slide';
     currentSlide = (currentSlide + 1) % slides.length;
     slides[currentSlide].className = 'slide showing';
 }

 let playing = true;
 let pauseButton = document.getElementById('pause');

 function pauseSlideshow() {
     pauseButton.innerHTML = 'Play';
     playing = false;
     clearInterval(slideInterval);
 }

 function playSlideshow() {
     pauseButton.innerHTML = 'Pause';
     playing = true;
     slideInterval = setInterval(nextSlide, 2000);
 }

 pauseButton.onclick = function() {
     if (playing) { pauseSlideshow(); } else { playSlideshow(); }
 };


 const allworks = document.querySelector('.allworks');
 const branding = document.querySelector('.branding');
 const design = document.querySelector('.design');
 const photography = document.querySelector('.photography');
 const allphoto = document.querySelectorAll('.grey-laer');




 function buttonClick(cssClass) {
     for (i = 0; i < allphoto.length; i++) {
         console.log(allphoto[i].dataset.filter)
         if (allphoto[i].dataset.filter != cssClass) {
             allphoto[i].style.display = "none";
         } else {
             allphoto[i].style.display = "block";
         }
     }
 }
 allworks.addEventListener('click', (e) => buttonClick('first'));
 branding.addEventListener('click', (e) => buttonClick('second'));
 design.addEventListener('click', (e) => buttonClick('third'));
 photography.addEventListener('click', (e) => buttonClick('fifth'));